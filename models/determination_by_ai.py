import pandas as pd
import numpy as np
from joblib import load
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler


def bincount2d_vectorized(a):
    n = a.max() + 1
    a_offs = a + np.arange(a.shape[0])[:, None] * n
    return np.bincount(a_offs.ravel(), minlength=a.shape[0] * n).reshape(-1, n)


def merge_to_dataset(dataset_train, data_frame, name_to_drop):
    return pd.merge(dataset_train, data_frame, how='left', left_index=True, right_index=True).drop([name_to_drop],
                                                                                                   axis=1)


class DeterminationByAi:
    def __init__(self, dataset_train, with_correct_column: bool = True):
        self.dataset_train = dataset_train
        self.with_correct_column = with_correct_column
        self.x_values = None
        self.y_values = None
        self.y_pred = None
        self.model = None
        self.matrix = None

    def predict(self):
        self.prepare_data()
        self.model = load('trained_model.joblib')
        self.y_pred = self.model.predict(self.x_values)
        self.dataset_train['PRED'] = self.y_pred.astype(int)
        if self.with_correct_column:
            self.dataset_train['CRRCT'] = self.y_values.astype(int)
            self.matrix = confusion_matrix(self.y_values, self.y_pred)
        self.dataset_train.to_csv(r'up-to-10-pers-game.csv', index=False)

    def prepare_data(self):
        self.count_suits()
        self.count_total_ranks()
        self.count_same_ranks()
        self.count_diff_between_ranks()
        self.split_dataset()
        self.normalize_data()

    def count_suits(self):
        data_frame = self.dataset_train.iloc[:, [0, 2, 4, 6, 8, 10, 12]].astype(int)
        data_frame = pd.DataFrame(bincount2d_vectorized(data_frame.values),
                                  columns=['suit0', 'suit1', 'suit2', 'suit3', 'suit4'])

        self.dataset_train = merge_to_dataset(self.dataset_train, data_frame, 'suit0')

    def count_total_ranks(self):
        data_frame = self.dataset_train.iloc[:, np.arange(1, 14, 2)].astype(int)
        cols = [f'rank{x}' for x in range(0, 14, 1)]
        data_frame = pd.DataFrame(bincount2d_vectorized(data_frame.values), columns=cols)

        self.dataset_train = merge_to_dataset(self.dataset_train, data_frame, 'rank0')

    def count_same_ranks(self):
        data_frame = self.dataset_train.loc[:, [f'rank{n}' for n in range(1, 14, 1)]].astype(int)
        data_frame = pd.DataFrame(bincount2d_vectorized(data_frame.values),
                                  columns=[f'rankCount{n}' for n in range(0, 5, 1)])

        self.dataset_train = merge_to_dataset(self.dataset_train, data_frame, 'rankCount0')

    def count_diff_between_ranks(self):
        self.dataset_train['diff1_13'] = self.dataset_train['rank1'] - self.dataset_train['rank13']

        for i in range(2, 14, 1):
            self.dataset_train[f'diff{i}_{i - 1}'] = self.dataset_train[f'rank{i}'] - self.dataset_train[f'rank{i - 1}']

    def split_dataset(self):
        if self.with_correct_column:
            self.x_values = self.dataset_train.drop(['CLASS'], axis=1).values
            self.y_values = self.dataset_train.CLASS.values
        else:
            self.x_values = self.dataset_train


    def normalize_data(self):
        sc = StandardScaler()
        self.x_values = sc.fit_transform(self.x_values)
