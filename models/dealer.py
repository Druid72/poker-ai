from models.table import Table
from models.card_deck import CardDeck
from models.player import Player


class Dealer:
    def __init__(self):
        self.card_deck: CardDeck = CardDeck()
        self.combination_names: [str] = ['Nothing in hand', 'One pair', 'Two pairs', 'Three of a kind', 'Straight',
                                         'Flush', 'Full house', 'Four of a kind', 'Straight flush', 'Royal flush']

    def put_cards_on_table(self, table: Table):
        for _ in range(5):
            table.put_card(self.card_deck.pop_card())

    def put_cards_to_player(self, player: Player):
        for _ in range(2):
            player.put_card(self.card_deck.pop_card())

    def announce_results(self):
        pass
