from models.card import Card


class Table:
    def __init__(self):
        self.cards: [Card] = []

    def put_card(self, card: Card):
        self.cards.append(card)
