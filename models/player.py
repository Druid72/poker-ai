from models.table import Table
from models.card import Card


class Player:
    def __init__(self, name):
        self.name: str = name
        self.hand: [Card] = []

    def put_card(self, card: Card):
        self.hand.append(card)

    def get_combination(self, table: Table) -> [Card]:
        return self.hand + table.cards
