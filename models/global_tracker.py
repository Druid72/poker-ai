import numpy as np
import pandas as pd

from models.card import Card
from models.determination_by_algorithm import DeterminationByAlgorithm


names_7_cards = ['S1', 'C1', 'S2', 'C2', 'S3', 'C3', 'S4', 'C4', 'S5', 'C5', 'S6', 'C6', 'S7', 'C7']


class GlobalTracker:
    def __init__(self):
        self.cards_to_check = np.array([]).astype(int)
        self.cards_dataset = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
        self.combinations_dataset = np.array([])
        self.games_dataset = np.array([])
        self.algorithm_result = None
        self.dataset = None
        self.game_id = None

    def set_game(self, game_id: int):
        self.game_id = game_id

    def append_combination(self, cards: [Card]):
        self.cards_to_check = np.array([]).astype(int)
        self.cards_to_check = np.append(self.cards_to_check, [[card.suit, card.rank] for card in cards])
        # self.check_player_cards()
        self.save_results()

    def check_player_cards(self):
        algorithm = DeterminationByAlgorithm(self.cards_to_check)
        algorithm.perform_check()
        self.algorithm_result = algorithm.result

    def save_results(self):
        self.cards_dataset = np.append(self.cards_dataset, [self.cards_to_check], axis=0)
        # self.combinations_dataset = np.append(self.combinations_dataset, [self.algorithm_result], axis=0)
        self.games_dataset = np.append(self.games_dataset, [self.game_id], axis=0)

    def create_data_frame(self):
        self.cards_dataset = self.cards_dataset[1:]
        self.dataset = pd.DataFrame(data=self.cards_dataset, columns=names_7_cards)
        # y_train_class = pd.DataFrame(self.combinations_dataset.astype(int), columns=['CLASS'])
        game_nums = pd.DataFrame(self.games_dataset.astype(int), columns=['GAME'])
        # self.dataset = pd.merge(self.dataset, y_train_class, how='left', left_index=True, right_index=True)
        self.dataset = pd.merge(self.dataset, game_nums, how='left', left_index=True, right_index=True)
