import random

from models.card import Card


class CardDeck:
    def __init__(self):
        self.cards: list[Card] = []
        self.generate_new_deck()

    def generate_new_deck(self):
        for suit in range(1, 5):
            [self.cards.append(Card(suit, rank)) for rank in range(1, 14)]

    def pop_card(self) -> Card:
        return self.cards.pop(random.randint(0, len(self.cards) - 1))
