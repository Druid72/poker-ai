import numpy as np
import pandas as pd
import pdb

ranks_of_cards = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King']
combinations = ['Nothing in hand', 'One pair', 'Two pairs', 'Three of a kind', 'Straight', 'Flush', 'Full house',
                'Four of a kind', 'Straight flush', 'Royal flush']


class DeterminationByAlgorithm:
    def __init__(self, cards):
        self.result = None
        self.combinations = None
        self.data_frame = None
        self.hearts = np.zeros((13,), dtype=int)
        self.spades = np.zeros((13,), dtype=int)
        self.diamonds = np.zeros((13,), dtype=int)
        self.clubs = np.zeros((13,), dtype=int)
        self.cards = cards - 1

    def perform_check(self):
        self.card_suit_check()
        self.create_dictionary_for_pandas_columns()
        self.find_combinations()
        self.calculate_best_result()

    def card_suit_check(self):
        for card_num in range(14):
            if card_num % 2 != 0:
                match self.cards[card_num - 1]:
                    case 0:
                        self.hearts[self.cards[card_num]] = 1
                    case 1:
                        self.spades[self.cards[card_num]] = 1
                    case 2:
                        self.diamonds[self.cards[card_num]] = 1
                    case 3:
                        self.clubs[self.cards[card_num]] = 1

    def create_dictionary_for_pandas_columns(self):
        suits = {"Hearts": pd.Series(self.hearts, index=ranks_of_cards),
                 "Spades": pd.Series(self.spades, index=ranks_of_cards),
                 "Diamonds": pd.Series(self.diamonds, index=ranks_of_cards),
                 "Clubs": pd.Series(self.clubs, index=ranks_of_cards)}

        self.data_frame = pd.DataFrame(suits)
        self.data_frame['combs'] = (self.data_frame == 1).sum(axis=1)

    def find_combinations(self):
        self.find_pairs()
        self.find_three_of_a_kind()
        self.find_four_of_a_kind()
        self.find_flush()
        self.find_full_house()
        self.find_straight()
        self.find_straight_flush()
        self.find_royal_flush()
        self.find_nothing_in_hand()

    def find_pairs(self):
        self.combinations = np.zeros((10,), dtype=int)
        pairs = (self.data_frame['combs'] == 2).sum(axis=0)
        if pairs == 1:
            # One pair
            self.combinations[1] = 1
        elif pairs >= 2:
            # Two pairs
            self.combinations[2] = 1

    def find_three_of_a_kind(self):
        three = (self.data_frame['combs'] == 3).sum(axis=0)
        if three >= 1:
            self.combinations[3] = 1

    def find_four_of_a_kind(self):
        self.combinations[7] = (self.data_frame['combs'] == 4).sum(axis=0)

    def find_flush(self):
        if (self.data_frame['Hearts'] == 1).sum(axis=0) >= 5 \
                or (self.data_frame['Spades'] == 1).sum(axis=0) >= 5 \
                or (self.data_frame['Diamonds'] == 1).sum(axis=0) >= 5 \
                or (self.data_frame['Clubs'] == 1).sum(axis=0) >= 5:
            self.combinations[5] = 1

    def find_full_house(self):
        if (self.combinations[1] != 0 or self.combinations[2] != 0) and self.combinations[3] != 0:
            self.combinations[6] = 1

    def find_straight(self):
        combs = self.data_frame['combs']
        a_int = (combs >= 1).astype(int)
        combs_sequence = 0
        for elem in range(len(a_int)):
            if elem > 11:
                if a_int[elem] == 1 and a_int[elem] == a_int[0]:
                    combs_sequence += 1
            elif a_int[elem] == 1 and a_int[elem] == a_int[elem + 1]:
                combs_sequence += 1
            else:
                combs_sequence = 0

            if combs_sequence == 4:
                self.combinations[4] = 1

    def find_straight_flush(self):
        if self.combinations[4] and self.combinations[5]:
            self.combinations[8] = 1

    def find_royal_flush(self):
        combs = self.data_frame['combs']
        a_int = (combs >= 1).astype(int)
        if a_int[0] == a_int[9] == a_int[10] == a_int[11] == a_int[12] == 1 and self.combinations[5]:
            self.combinations[9] = 1

    def find_nothing_in_hand(self):
        if (self.combinations == 1).sum(axis=0) == 0:
            self.combinations[0] = 1

    def calculate_best_result(self):
        self.combinations = self.combinations[::-1]
        index = np.nonzero(self.combinations == 1)
        self.result = 9 - index[0][0]
