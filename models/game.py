import random

from models.player import Player
from models.dealer import Dealer
from models.table import Table
from models.card_deck import CardDeck
from models.card import Card


class Game:
    def __init__(self, game_id: int):
        self.id = game_id
        self.dealer: Dealer = Dealer()
        self.table: Table = Table()
        self.card_deck: CardDeck = CardDeck()

        self.player_names_sample: [str] = ['1st player', '2nd player', '3rd player', '4th player', '5th player',
                                           '6th player', '7th player', '8th player', '9th player', '10th player']
        self.players: list[Player] = [Player(self.player_names_sample[i]) for i in range(random.randint(2, 10))]
        self.current_player_number: int = 0
        self.current_player: Player = self.players[self.current_player_number]
        self.players_combinations: [Player, [Card]] = []

    def play_round(self):
        self.dealer.put_cards_on_table(self.table)

        while True:
            self.dealer.put_cards_to_player(self.current_player)
            if self.current_player_number == len(self.players) - 1:
                break

            self.move_to_next_player()

        self.check_combinations()

    def move_to_next_player(self):
        self.current_player_number += 1
        self.current_player = self.players[self.current_player_number]

    def check_combinations(self):
        for player in self.players:
            self.players_combinations.append([player, player.get_combination(self.table)])
